﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordFinder
{
    public class RepeatedWord
    {
        public string Word { get; set; }
        public int TimesRepeated { get; set; }
        
    }
}
